/*
 * This calls is facilitator to Application storage.
 * That includes easy access to Cookies, Session and LocalStorage
 */

export default class Storage {

  static setCookie (name, value, expires=undefined, domain = undefined, path=undefined) {
    let cookie = `${name}=${value}`
    if (expires) {
      cookie = cookie + `;expires=${expires}`
    }
    if (domain) {
      cookie = cookie + `;domain=${domain}`
    }
    if (path) {
      cookie = cookie + `;path=${path}`
    }
    document.cookie = cookie
  }

  static getCookie (cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let cookieArray = decodedCookie.split(';');
    for(let cookie of cookieArray) {
      while (cookie.charAt(0) == ' ') {
        cookie = cookie.substring(1);
      }
      if (cookie.indexOf(name) == 0) {
        return cookie.substring(name.length);
      }
    }
    return "";
  }

  static deleteCookie (name, domain = '') {
    if (document.cookie.includes(`${name}=`)) {
      document.cookie = `${name}= ; expires = Thu, 01 Jan 1970 00:00:00 GMT; domain=${domain};`
      return true
    }
    return false
  }
}
