import Vue from "vue"
import VueAxios from "vue-axios"
import Storage from "@/utilities/storage.js"
import axios from "axios"
import store from "@/store"
import { v4 as uuidv4 } from 'uuid'

Vue.use(VueAxios, axios)

export default class Authenticator {

  loggInFromStorage () {
    let accessToken = Storage.getCookie('acctkn')
    if (accessToken) {
      Vue.prototype.axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`

      var jwtPayload = accessToken.split('.')[1]
      var jsonPayloadStr = atob(jwtPayload)
      var userData = JSON.parse(jsonPayloadStr)

      store.commit('setUserData', userData.identity)
      store.commit('setUserLoggedInFlag', true)
      store.commit('setUserAccessToken', accessToken)
    }

    let anonymousId = Storage.getCookie('anonymous_id')
    if (!anonymousId) {
      var now = new Date()
      const TEN_YEARS_MS = 1000*3600*24*365*10
      now.setTime(now.getTime() + TEN_YEARS_MS)
      var domain = `.${document.location.host}`
      var path = '/'

      Storage.setCookie('anonymous_id', uuidv4(), now.toGMTString(), domain, path)
    }
  }
}
